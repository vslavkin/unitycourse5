﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] List<WaveConfig> waveConfigs;
    [SerializeField] int startingWave = 0;
    [SerializeField] bool looping = false;


    // Start is called before the first frame update
    IEnumerator Start()
    {
        do
        {
            yield return StartCoroutine(SpawnAllWaves());
        }
        while (looping);
        
        
    }

    private IEnumerator SpawnAllWaves()
    {
        for(int waveIndex = startingWave; waveIndex < waveConfigs.Count; waveIndex++)
        {
            var currentWave = waveConfigs[Random.Range(0, waveConfigs.Count)];
            yield return StartCoroutine(SpawnWave(currentWave));
        } 
    }


    private IEnumerator SpawnWave(WaveConfig waveConfig)
    {
        for(int enemyCount = 0; enemyCount < waveConfig.GetNumberOfEnemies(); enemyCount++)
        {
            var newEnemy = Instantiate(
            waveConfig.GetEnemyPrefab(),
            waveConfig.GetWaypoints()[0].transform.position,
            waveConfig.GetEnemyPrefab().transform.rotation);

            newEnemy.GetComponent<EnemyPathing>().setWaveConfig(waveConfig);
            yield return new WaitForSeconds
                (Random.Range
                (waveConfig.GetSpawnCooldown()-(waveConfig.GetSpawnRandomness() /2),
                 waveConfig.GetSpawnCooldown()+(waveConfig.GetSpawnRandomness() /2)));
        }
        
    }
}
