﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DamageDealer : MonoBehaviour
{
    [SerializeField] int Damage = 100;
    [SerializeField] bool isbullet = true;

    public int GetDamage()
    {
        return Damage;
    }

    public void Hit()
    {
        Destroy(gameObject);
    }

    public bool Getbullet()
    {
        return isbullet;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if(!damageDealer){return;}
        if(damageDealer.Getbullet()){
            damageDealer.Hit();
            if(isbullet){Hit();}
        }
    }
}

