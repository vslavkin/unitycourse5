﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickup : MonoBehaviour
{
    [SerializeField] int HPRecovered;
    [SerializeField] float DisappearingTime;
    [SerializeField] float AlertTime;
    [SerializeField] Animator animator;
    [SerializeField] bool destructible;
    bool counting = false;
    float time;

    private void Start() {
        counting = true;
    }

    public void SetHPTo(int hp){
        HPRecovered = hp;
    }

    private void Update() {
        if (counting) {
            if(time >= AlertTime){
                animator.SetBool("Disappearing", true);
            }
            if (time <= DisappearingTime) {
                time += Time.deltaTime;
            }
            else{
                Destroy(gameObject);
            }
        }
    }

    public int GetHP(){
        return HPRecovered;
    }

    public void Used(){
        Destroy(gameObject);
    }

private void OnTriggerEnter2D(Collider2D other) {
    DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
    if(!damageDealer){return;}
    if(damageDealer.Getbullet()){
        damageDealer.Hit();
        Destroy(gameObject);
    }
}
}