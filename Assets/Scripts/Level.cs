using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void LoadSceneWithDelay(string scene, int delay)
    {
        StartCoroutine(Delay(delay, scene));    
    }

    public void ResetScore(){
        if(FindObjectOfType<gameSession>())
        FindObjectOfType<gameSession>().ResetGame(); 
    }

    IEnumerator Delay(int delay, string scene)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(scene);    
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
