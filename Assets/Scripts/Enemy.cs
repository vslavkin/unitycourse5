﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Enemy : MonoBehaviour
{

    [Header("Values")]
    [SerializeField] int maxHP;
    [SerializeField] int Damage;
    int[] HP;
    [SerializeField] float shotCounter;
    [SerializeField] float minShootCooldown = 0.2f;
    [SerializeField] float maxShootCooldown = 2f;

    [Header("Shooting")]
    [SerializeField] GameObject projectile;
    [SerializeField] float projectileSpeed = 10f;

    [Header("Death")]
    [SerializeField] GameObject deathVFX;
    [SerializeField] float explosionDuration;

    [Header("Sound")]
    [SerializeField] List<AudioClip> deathSound;
    [SerializeField] [Range(0, 1)] float deathSoundVolume = 0.7f;
    [SerializeField] List<AudioClip> shootSound;
    [SerializeField] [Range(0, 1)] float shootSoundVolume = 0.25f;

    [Header("Score")]
    [SerializeField] int score;

    [Header("heart")]
    [SerializeField] GameObject heart;
    [SerializeField] float probability;

    public HealthSlider healthslider;
    
    gameSession GameSession;


    // Start is called before the first frame update
    void Start()
    {
        HP = new int[2];
        HP[0] = maxHP;
        HP[1] = HP[0];
        GameSession = FindObjectOfType<gameSession>();
        shotCounter = Random.Range(minShootCooldown, maxShootCooldown);
        if (healthslider){healthslider.SetMaxHealth(maxHP);}
    }

    // Update is called once per frame
    void Update()
    {
        Counter();
    }

    private void Counter()
    {
        shotCounter -= Time.deltaTime;
        if(shotCounter <= 0f)
        {
            Shoot();
            shotCounter = Random.Range(minShootCooldown, maxShootCooldown);
        }
    }

    private void Shoot()
    {
        if (projectile)
        {
            GameObject laser = Instantiate(
            projectile,
            transform.position,
            transform.rotation)
            as GameObject;
            laser.GetComponent<Rigidbody2D>().velocity = laser.transform.up * projectileSpeed * -1;
            AudioSource.PlayClipAtPoint(shootSound[Random.Range(0, shootSound.Count)], Camera.main.transform.position, shootSoundVolume);
        }
        else return;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        Player player = other.gameObject.GetComponent<Player>();
        if (damageDealer) { 
            ProcessHit(damageDealer);
            return;      
        }
        else if (player){
            ProcessHit(player.GetHP(1));
        }

    }

    private void ProcessHit(DamageDealer damageDealer)
    {
        HP[0] -= damageDealer.GetDamage();
        HP[1] = HP[0];
        if (healthslider){healthslider.SetHealth(HP[0]);};
        if (damageDealer.Getbullet())
        {
            damageDealer.Hit();
        }
        if (HP[0] <= 0)
        {
            Die();
        }
    }

    public void ProcessHit(int damage){
        HP[0] = Mathf.Clamp(HP[0] - damage, 0,maxHP);
        HP[1] = HP[0];
        if(healthslider){healthslider.SetHealth(HP[0]);}
        if(HP[0] <= 0){
            Die();
        }
    }

    private void Die()
    {
        GameSession.AddScore(score);
        Destroy(gameObject);
        SpawnHearth();
        GameObject explosion = Instantiate(deathVFX, transform.position, transform.rotation);
        Destroy(explosion, explosionDuration);
        AudioSource.PlayClipAtPoint(
            deathSound[Random.Range(0, deathSound.Count)],
            Camera.main.transform.position, 
            deathSoundVolume );
    }

    private void SpawnHearth(){
        if(Random.value <= (probability/100)){
            Instantiate(heart, transform.position, transform.rotation);
        }
    }

public int GetHP(int i){return HP[i];}
public int GetDamage(){ return Mathf.Clamp(Damage-(maxHP - HP[1]),1,Damage); }

}