﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class scoreDisplay : MonoBehaviour
{
  TextMeshProUGUI scoreText;
gameSession gameSession;

[SerializeField]  float H,S,V;
[SerializeField]  float step = 0.0075f;

string scoreTitle = "Score:";

  void Start()
  {
    scoreText = GetComponent<TextMeshProUGUI>();
    gameSession = FindObjectOfType<gameSession>();
  }

  void FixedUpdate(){
      scoreText.text = scoreTitle + gameSession.GetScore().ToString();
    if (gameSession.GameOver){
      RGB();
    }
  }
  public void RGB(){
        Color.RGBToHSV(scoreText.color,out H, out S, out V);
        H+=step;
        scoreText.color = Color.HSVToRGB(H,S,V);
}
}
