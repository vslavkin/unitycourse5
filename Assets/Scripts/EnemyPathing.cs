using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour
{
    WaveConfig waveConfig;
    List<Transform> waypoints;
    int waypointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        waypoints = waveConfig.GetWaypoints(); 
        transform.position = waypoints[waypointIndex].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    public void setWaveConfig(WaveConfig waveConfig)
    {
        this.waveConfig = waveConfig;
    }

    private void Move()
    {
        if (waypointIndex <= (waypoints.Count - 1))
        {
            var targetTransform = waypoints[waypointIndex].transform;
            var movementThisFrame = waveConfig.GetMoveSpeed() * Time.deltaTime;
            var rotationThisFrame = waveConfig.GetRotationSpeed()* Time.deltaTime;
            transform.position = Vector2.MoveTowards
                (transform.position, targetTransform.position, movementThisFrame);
            transform.rotation = Quaternion.RotateTowards
                                (transform.rotation, targetTransform.rotation, rotationThisFrame);
            
            if (transform.position == targetTransform.position && transform.rotation == targetTransform.rotation)
            {
                waypointIndex++;
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
