using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    [Header("Player Movement")]
    [SerializeField] float movementSpeed = 10f;
    [SerializeField] float xPadding = 1f;
    [SerializeField] float yPadding = 1f;

    [SerializeField] int MaxHP = 20;
    int[] HP;


    [Header("Sound")]
    [SerializeField] [Range(0, 1)] float deathSoundVolume = 0.7f;
    [SerializeField] [Range(0, 1)] float shootSoundVolume = 0.25f;
    [SerializeField] List<AudioClip> deathSound;
    [SerializeField] List<AudioClip> shootSound;
    [SerializeField] List<AudioClip> HPPickupSound;


    [Header("Projectile")]
    [SerializeField] GameObject laserPrefab;
    [SerializeField] float projectileSpeed = 10f;
    [SerializeField] float shootingCooldown = 0.5f;
    [SerializeField] GameObject[] canon;
    [SerializeField] int shootingcanon;
    bool isShooting;
    Coroutine firingCoroutine;

    [Header("VFX")]
    [SerializeField] GameObject DeathVFX;
    [SerializeField] float Duration;

    //----------------------------------------------------------------
    //Movement Properties
    Vector2 m_Move;
    float xMin;
    float xMax;
    float yMin;
    float yMax;
    float timer;
    bool timerIsRunning;

    //----------------------------------------------------------------
    //* References
    gameSession GameSession;

    public HealthSlider healthslider;

    //----------------------------------------------------------------
    void Start()
    {
        SetUpMoveBoundaries();
        timer = shootingCooldown;
        timerIsRunning = true;
        GameSession = FindObjectOfType<gameSession>();
        healthslider.SetMaxHealth(MaxHP);
        HP = new int[2];
        HP[0] = MaxHP;
        HP[1] = HP[0];
    }
    private void SetUpMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0,0,0)).x + xPadding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - xPadding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + yPadding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - yPadding;
    }
    void Update()
    {
        Move();
        Fire();
        Timer();

    }
    IEnumerator ContinuousFire()
    {
        while (true)
        {
            if (shootingcanon == canon.Length - 1)
            {
                shootingcanon = 0;
            }
            else
            {
                shootingcanon++;
            }
            GameObject laser = Instantiate(laserPrefab, canon[shootingcanon].transform.position, canon[shootingcanon].transform.rotation) as GameObject;
            laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);
            AudioSource.PlayClipAtPoint(shootSound[Random.Range(0, shootSound.Count)], Camera.main.transform.position, shootSoundVolume);
            yield return new WaitForSeconds(shootingCooldown);
        }
            
        
        
    }
    public void Timer()
    {
        if (timerIsRunning)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;

            }
            else
            {
                timerIsRunning = false;
                timer = shootingCooldown;
            }
        }
       
    }
    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (!timerIsRunning)
            {
                if (!isShooting)
                {
                    firingCoroutine  =  StartCoroutine(ContinuousFire());
                    isShooting = true;
                }
                timerIsRunning = true;
            }
            
            
        }

        if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(firingCoroutine);
            isShooting = false;
            //StopAllCoroutines();
        }
        else{return;}
    }
    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * movementSpeed;
        var deltaY = Input.GetAxis("Vertical")   * Time.deltaTime * movementSpeed;


        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);
        var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);


        transform.position = new Vector2(newXPos, newYPos);
        //transform.position.x=newXPos;
        //transform.position.y=newYPos;
        //transform.position.Set(newXPos, newYPos,transform.position.z);
        //Debug.Log();



    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        HealthPickup hpickup = other.gameObject.GetComponent<HealthPickup>();
        Enemy enemy = other.gameObject.GetComponent<Enemy>();
        if (damageDealer) {
            ProcessHit(damageDealer);
        }
        else if (hpickup) {
            AddHP(hpickup);
        }
        else if (enemy){
            ProcessHit(enemy.GetDamage());
        } 
    }
    private void ProcessHit(DamageDealer damageDealer)
    {
        HP[0] -= damageDealer.GetDamage();
        healthslider.SetHealth(HP[0]);
        if (damageDealer.Getbullet())
        {
            damageDealer.Hit();
        }

        if (HP[0] <= 0)
        {
            Die();
        }
    }

    public void ProcessHit(int damage){
        HP[1] = HP[0];
        HP[0] = Mathf.Clamp(HP[0] - damage, 0, MaxHP);
        healthslider.SetHealth(HP[0]);
        if(HP[0] <= 0){
            Die();
        }
    }
    private void Die()
    {
        Level level = GameObject.FindObjectOfType<Level>();
        level.LoadSceneWithDelay("GameOver", 3);
        Destroy(gameObject);
        GameObject explosion = Instantiate(DeathVFX, transform.position, transform.rotation);
        Destroy (explosion, Duration);
        AudioSource.PlayClipAtPoint(deathSound[Random.Range(0,deathSound.Count)], Camera.main.transform.position, deathSoundVolume);
        GameSession.GameOver = true;

    }
    public void AddHP(HealthPickup hpickup){
        if(HP[0] < MaxHP){
            int addedHP = hpickup.GetHP();
            HP[0] = Mathf.Clamp(HP[0] + addedHP, 0, MaxHP);
            healthslider.SetHealth(HP[0]);
            AudioSource.PlayClipAtPoint(HPPickupSound[Random.Range(0,HPPickupSound.Count)], Camera.main.transform.position);
            hpickup.Used();
        }
    }

public int GetMaxHP(){return MaxHP;}

public int GetHP(int i){ return HP[i];}


}


