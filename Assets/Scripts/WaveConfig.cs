﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(menuName = "Enemy Wave Config")]

public class WaveConfig : ScriptableObject
{

    [SerializeField] GameObject enemyPrefab;
    [SerializeField] GameObject pathPrefab;
    [SerializeField] float spawnCooldown = 0.5f;
    [SerializeField] float spawnRandomness = 0.3f;
    [SerializeField] int numberOfEnemies = 5;
    [SerializeField] float moveSpeed = 2f;

    [SerializeField] float rotationSpeed = 10f;

    public GameObject GetEnemyPrefab()   { return enemyPrefab;}

    public List<Transform> GetWaypoints() 
    {
        var waveWaypoints = new List<Transform>();
        foreach(Transform child in pathPrefab.transform)
        {
            waveWaypoints.Add(child);
        }

        return waveWaypoints; 
    }

    public float GetSpawnCooldown() { return spawnCooldown; }

    public float GetSpawnRandomness() { return spawnRandomness; }

    public float GetNumberOfEnemies() { return numberOfEnemies; }

    public float GetMoveSpeed() { return moveSpeed; }

    public float GetRotationSpeed() { return rotationSpeed; }

}
