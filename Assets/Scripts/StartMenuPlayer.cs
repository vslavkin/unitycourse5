using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuPlayer : MonoBehaviour
{


    [Header("Sound")]
    [SerializeField] [Range(0, 1)] float shootSoundVolume = 0.25f;
    [SerializeField] List<AudioClip> shootSound;


    [Header("Projectile")]
    [SerializeField] GameObject laserPrefab;
    [SerializeField] float projectileSpeed = 10f;
    [SerializeField] float shootingCooldown = 10f;


    [SerializeField] float timer;

    [SerializeField] GameObject[] canon;
    [SerializeField] int shootingcanon;

    // Start is called before the first frame update
    void Start()
    {
        timer = shootingCooldown;

    }


    // Update is called once per frame
    void Update()
    {
        Timer();

    }


    public void Timer()
    {
   
            if (timer >= 0.0f)
            {
                timer -= Time.deltaTime;
          //  Debug.Log("todavia no se llego al tiempo");

            }
            else
            {
          //  Debug.Log("Disparando...");
            timer = shootingCooldown;
            if (shootingcanon == canon.Length - 1)
            {
                shootingcanon = 0;
            }
            else
            {
                shootingcanon++;
            }
            StartCoroutine(Fire());

        }
       
            
        

    }


    IEnumerator Fire()
    {

        
        GameObject laser = Instantiate(laserPrefab, canon[shootingcanon].transform.position, canon[shootingcanon].transform.rotation) as GameObject;
        laser.GetComponent<Rigidbody2D>().velocity = laser.transform.up * projectileSpeed;
        AudioSource.PlayClipAtPoint(shootSound[Random.Range(0,shootSound.Count)], Camera.main.transform.position, shootSoundVolume);
        yield return new WaitForSeconds(3f);
        Destroy(laser);
        yield break;


    }
    }


