﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameSession : MonoBehaviour
{
    int score;
    public bool GameOver;



    void Awake()
    {
        Singletone();
    }
    void Singletone()

    { 
       int nOfSessions = FindObjectsOfType(GetType()).Length;
       if (nOfSessions > 1)
       {
           Destroy(gameObject);
       } 
       else
       {
           DontDestroyOnLoad(gameObject);
       }
    }
    private void Start() {

        GameOver = false;
    }
   public int GetScore()

   {
       return score;
   }
   public void AddScore(int Ascore)

   {
       score += Ascore;
   }
   public void ResetGame()

   {
       Destroy(gameObject);
   }

}
